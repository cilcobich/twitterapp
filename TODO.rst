TODO
====

* Save tweet date value in a DateTimeField field in db
* Display tweetline in a fancy format

DONE
====

* Identify user with username (check in db) 
* Create test folder
* Save oauth_token and oauth_token_secret in user's table
* Ask for pin method and register user in database
