from oauthtwitter import OAuthApi
from models import User, Tweet
from functools import wraps
from urllib2 import HTTPError
import time
from datetime import datetime


class LoginRequired(Exception):
    pass


class InvalidPin(Exception):
    pass


def login_required(fn):
    @wraps(fn)
    def wrapped(*args, **kwargs):
        if not args[0].logged:
            raise LoginRequired('Login required.')
        return fn(*args, **kwargs)
    return wrapped


class TwitterApp():
    def __init__(self, ck, cs):
        self.consumer_key = ck
        self.consumer_secret = cs
        self.app_api = OAuthApi(self.consumer_key, self.consumer_secret)
        self.temp_credentials = None
        self.user_api = None
        self.logged = False
        self.tweets_list = []
        self.user = None

    def get_authorization_url(self):
        # User pastes this into their browser to bring back a pin number
        self.temp_credentials = self.app_api.getRequestToken()
        url = self.app_api.getAuthorizationURL(self.temp_credentials)
        return url

    def get_access_token(self, pin):
        """
            Recieves a pin number and tries to get oauth_token
            and oauth_token_secret credentials
        """
        access_tokens = self.app_api.getAccessToken(self.temp_credentials, pin)

        if all(k in access_tokens for k in ('screen_name',
                                            'user_id',
                                            'oauth_token',
                                            'oauth_token_secret')):
            return access_tokens

        raise InvalidPin()

    def oauth_login(self, oauth_token, oauth_token_secret):
        """
            Instantiates an authenticated OAuthApi
            object with the given oauth credentials
        """
        self.user_api = OAuthApi(self.consumer_key, self.consumer_secret,
                                 oauth_token, oauth_token_secret)

    def register_user(self, access_tokens):
        User.create(
            id_user=access_tokens['user_id'],
            screen_name=access_tokens['screen_name'],
            oauth_token=access_tokens['oauth_token'],
            oauth_token_secret=access_tokens['oauth_token_secret']
        )
        return True

    def check_username(self, username):
        """
            Checks for a given username in database
        """
        return User.check_username_exists(username)

    def login(self, username):
        self.user = User.get(User.screen_name == username)
        self.oauth_login(self.user.oauth_token, self.user.oauth_token_secret)
        self.logged = True

    @login_required
    def get_data_profile(self):
        result = {}
        parameters = {'user_id': self.user.user_id, 'include_entities': 'true'}
        try:
            dic = self.user_api.ApiCall("users/lookup", "GET", parameters)[0]
        except HTTPError as error:
            result = {'error': error.message}
            return result

        result['name'] = dic['name']
        result['screen_name'] = dic['screen_name']
        result['friends_count'] = dic['friends_count']
        result['followers_count'] = dic['followers_count']
        result['statuses_count'] = dic['statuses_count']
        result['last_publication'] = dic['status']['text']

        return result

    @login_required
    def tweet(self, text):
        """
           Updates status and returns whether
           or not the status update succeded.
        """
        try:
            succ = self.user_api.UpdateStatus(text)
        except HTTPError:
            return False

        return succ

    @login_required
    def update_timeline(self):
        self._sync_tweetline()
        tweets_list = self.user.get_tweets()
        result = [{'screen_name': '@{0}'.format(tweet_object.user_screen_name),
                   'text': tweet_object.text,
                   'created_at': tweet_object.created_at
                   } for tweet_object in reversed(tweets_list)]

        return result

    def _parse_tweets(self, tweets_response_object):
        """
            Recieves a json response of tweets, in the format:
            https://dev.twitter.com/docs/api/1.1/get/statuses/user_timeline, and
            returns a list of Tweet objects
        """
        tweets_list = []
        for tweet in tweets_response_object:
            # Convert string date to datetime object
            str_date = tweet['created_at']
            t = time.strptime(str_date, '%a %b %d %H:%M:%S +0000 %Y')
            tweet['created_at'] = datetime.fromtimestamp(time.mktime(t))
            tweets_list.append(
                            Tweet(
                                id_tweet=tweet['id'],
                                created_at=tweet['created_at'],
                                user_name=tweet['user']['name'],
                                user_screen_name=tweet['user']['screen_name'],
                                text=tweet['text'],
                                user=self.user
                                )
                        )
        return tweets_list

    def _sync_tweetline(self):
        '''Get new tweets since last tweet fetched,
        if last_tweet_id is None, will retrieve the last 20 tweets'''

        last_tweet_id = self.user.get_last_tweet_id()
        request_params = {'since_id': last_tweet_id}

        if last_tweet_id is None:
            # retrieve the last 20 tweets
            request_params = {'count': 20}

        tweets_json = self.user_api.GetHomeTimeline(request_params)
        tweets_list = self._parse_tweets(tweets_json)

        for tweet in reversed(tweets_list):
            tweet.save() # persist all retrived tweets

    def logout(self):
        """Resets all instance variables"""
        self.__init__(self.consumer_key, self.consumer_secret)
