import subprocess
from clint.textui import colored, indent, puts, columns
from twitterapp import TwitterApp, InvalidPin, LoginRequired
from functools import wraps

CONSUMER_KEY = "OWR5dtoVdzQinmvFANFGg"
CONSUMER_SECRET = "XzaKMkoYAkB7KWMNyHRBpgLZD1tHlOzKEczBz6sMA"

t = TwitterApp(CONSUMER_KEY, CONSUMER_SECRET)


def cmd_login_required(fn):
    @wraps(fn)
    def wrapped(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except LoginRequired:
            puts(colored.red('You must be logged in'))
    return wrapped



def cmd_register():
    confirm = raw_input("You'll now be redirected to twitter's login page"
                        "\nyou'll get a <pin number> wich you'll have"
                        "\nto insert to use this application"
                        "\nDo you accept? (y/n) ").strip()
    if confirm == 'n':
        return

    url = t.get_authorization_url()
    subprocess.call(['firefox', url])
    pin = raw_input('What is the PIN? ').strip()
    try:
        access_tokens = t.get_access_token(pin)
        t.register_user(access_tokens)
        t.login(access_tokens['screen_name'])
        puts(colored.green('Login success full.'))

    except InvalidPin:
        puts(colored.red('pin: %s invalid. Try again.\n' % pin))


@cmd_login_required
def cmd_check_tweetfeed():
    tweets_list = t.update_timeline()
    puts()
    col1 = 5
    col2 = 50

    for tweet in reversed(tweets_list):
        screen_name = tweet['screen_name']
        text = tweet['text']
        created_at = tweet['created_at'].strftime('%a %d %b %H:%M').rjust(29, ' ')
        puts(columns([colored.yellow(''.rjust(col1, '-')), col1], [colored.yellow(unicode(created_at).encode('utf-8')), col2]))
        puts(columns([colored.yellow(''.rjust(col1, '-')), col1], [colored.red(unicode(screen_name).encode('utf-8')), col2]))
        puts(columns(['', col1], [colored.white(unicode(text).encode('utf-8')), col2]))
        puts()
    puts()

@cmd_login_required
def cmd_tweet():
    if not t.logged:
        raise LoginRequired
    tweet = raw_input('Compose new tweet...\n')
    t.tweet(tweet)
    print 'Tweet: ''%s ''send.' % tweet


def cmd_login():
    username = raw_input("Please enter your Twitter username > ")
    if t.check_username(username):
        t.login(username)
        puts(colored.green('Welcome {0}'.format(username)))

    else:
        puts(colored.red("Can't find username {0}".format(username)))
        puts(colored.blue("Please verify it is correct. Otherwise register."))

menu = {
    '1': cmd_login,
    '2': cmd_register,
    '3': cmd_tweet,
    '4': cmd_check_tweetfeed,
}


def dispatch(option):
    """
        Simon says: do option
    """
    puts('seleccionaste %s' % option)

    if option in menu:
        menu[option]()


option = 0
puts(colored.red('WELCOME STRANGER'))

while option != 'q':
    puts(colored.green('\nPlease select an option'))
    with indent(4, quote=colored.blue(' >')):
        puts('1) Login')
        puts('2) Register')
        puts('3) New Tweet')
        puts('4) Check tweetfeed')
        puts('q) Quit')
    option = raw_input().strip()
    dispatch(option)


puts(colored.blue('Bon voyage!'))
