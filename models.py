from peewee import Model, SqliteDatabase, CharField, ForeignKeyField, DateTimeField

DATABASE = 'db_twitterapp.db'

# create a peewee database instance -- our models will use this database to
# persist information
database = SqliteDatabase(DATABASE)

# model definitions -- the standard "pattern" is to define a base model class
# that specifies which database to use. then, any subclasses will automatically
# use the correct storage. for more information, see:
# http://charlesleifer.com/docs/peewee/peewee/models.html#model-api-smells-like-django


class BaseModel(Model):
    class Meta:
        database = database


class User(BaseModel):
    id_user = CharField()
    screen_name = CharField()
    oauth_token = CharField()
    oauth_token_secret = CharField()

    @classmethod
    def check_username_exists(cls, username):
        """
            Checks for a given username in database
        """
        try:
            User.get(User.screen_name == username)
        except User.DoesNotExist:
            return False
        return True

    def get_tweets(self):
        tweets_query = Tweet.select().where(Tweet.user == self)

        twees_list = []
        for tweet_objects in tweets_query:
            twees_list.append(tweet_objects)
        return twees_list

    def get_last_tweet_id(self):
        last_tweet_query = Tweet.select().where(Tweet.user == self).order_by(Tweet.id.desc()).limit(1)
        if last_tweet_query.count() > 0:
            for tweet_objects in last_tweet_query:
                return tweet_objects.id_tweet
        else:
            return None


class Tweet(BaseModel):
    id_tweet = CharField()
    created_at = DateTimeField()
    user_name = CharField()
    user_screen_name = CharField()
    text = CharField()
    user = ForeignKeyField(User, related_name='tweet')
