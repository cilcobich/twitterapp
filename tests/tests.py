import unittest
import simplejson as json
from mock import MagicMock
import sys
import os
from urllib2 import HTTPError

current_dir = os.getcwd()
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)
sys.path.insert(0, current_dir)

from twitterapp import TwitterApp, InvalidPin, LoginRequired, login_required
from models import User, Tweet
from oauthtwitter import OAuthApi


CONSUMER_KEY = "OWR5dtoVdzQinmvFANFGg"
CONSUMER_SECRET = "XzaKMkoYAkB7KWMNyHRBpgLZD1tHlOzKEczBz6sMA"


class DumbClass(object):
    """
        Class used to test the login_required decorator.
    """

    def __init__(self, logged=False):
        self.logged = logged

    @login_required
    def dumb_method(self):
        pass

MODELS = [User, Tweet]


def alter_tables(method='create_table'):
    """
        Helper function to create/delete tables.
    """
    for model in MODELS:
        getattr(model, method)()


class TestTwitterApp(unittest.TestCase):

    def setUp(self):
        alter_tables('create_table')
        self.t = TwitterApp('123', 'abc')
        self.access_token = {'oauth_token': '1089745705-NgX3Ayo3L3LVLVnJb5iNcXOEApVlxSxMgAWiEJ8',
                             'oauth_token_secret': 'pMUnFQFEOE5Gm7wCYXnUW2DHMocwRud4WxTknBJhOc',
                             'screen_name': 'DeveloFelix',
                             'user_id': '1089745705'}

    def test_init(self):
        self.assertEqual(self.t.consumer_key, '123')
        self.assertEqual(self.t.consumer_secret, 'abc')
        self.assertIsInstance(self.t.app_api, OAuthApi)
        self.assertIsNone(self.t.temp_credentials)
        self.assertIsNone(self.t.user_api)
        self.assertIsNone(self.t.user)
        self.assertFalse(self.t.logged)
        self.assertEqual(self.t.tweets_list, [])

    def test_authorization_url(self):
        temp_credentials = {'oauth_callback_confirmed': 'true',
                            'oauth_token': 'dRXTXs3rFNibOmihPnJadtaYevWXQBPsA2VIQunOLts',
                            'oauth_token_secret': 'VrNNXkrW6M1zuVNOAWSOTOuu3vR2pcfJiyq2fyHPw'}
        self.t.app_api.getRequestToken = MagicMock(return_value=temp_credentials)
        url = self.t.get_authorization_url()
        self.assertEqual(url, 'http://twitter.com/oauth/authorize?oauth_token=dRXTXs3rFNibOmihPnJadtaYevWXQBPsA2VIQunOLts')

    def test_get_access_token(self):
        self.t.app_api.getAccessToken = MagicMock(return_value=self.access_token)
        self.assertEqual(self.access_token, self.t.get_access_token('123'))

    def test_get_access_token_raises_invalidPin(self):
        access_token = {}
        self.t.app_api.getAccessToken = MagicMock(return_value=access_token)
        self.assertRaises(InvalidPin, self.t.get_access_token, '123')

    def test_oauth_login(self):
        """
            Oauth_login should create an OAuthApi object in TwitterApp.user_api.
        """
        self.t.oauth_login('abc', '123')
        self.assertIsInstance(self.t.user_api, OAuthApi)

    def test_register_user(self):
        self.assertTrue(self.t.register_user(self.access_token))

    def test_check_username_exists(self):
        User.create(screen_name="test_user", id_user='', oauth_token='', oauth_token_secret='')
        self.assertTrue(self.t.check_username('test_user'))

    def test_check_username_not_exists(self):
        self.assertFalse(self.t.check_username('test_user'))

    def test_login(self):
        """
            After login method is executed
            we must have an instance of the logged user, an
            instance of the authorized twitter api and the
            logged flag set to true.
         """
        self.t.oauth_login('abc', '123')
        User.create(screen_name="test_user", id_user='', oauth_token='', oauth_token_secret='')
        self.t.login('test_user')
        self.assertIsInstance(self.t.user, User)
        self.assertIsInstance(self.t.user_api, OAuthApi)
        self.assertTrue(self.t.logged)

    def test_tweet(self):
        try:
            with open('test_tweet_update_response.json') as f:
                response = json.load(f)
        except IOError:
            os.chdir(os.path.join(os.getcwd(), 'tests'))
            with open('test_tweet_update_response.json') as f:
                response = json.load(f)
        self.t.oauth_login('abc', '123')
        User.create(screen_name="test_user", id_user='', oauth_token='', oauth_token_secret='')
        self.t.login('test_user')
        self.t.app_api.UpdateStatus = MagicMock(return_value=response)
        self.assertTrue(self.t.tweet)

    def test_sync_tweetline(self):
        try:
            with open('test_tweets_response.json') as f:
                response = json.load(f)
        except IOError:
            os.chdir(os.path.join(os.getcwd(), 'tests'))
            with open('test_tweets_response.json') as f:
                response = json.load(f)
        self.t.oauth_login('abc', '123')
        User.create(screen_name="test_user", id_user='', oauth_token='', oauth_token_secret='')
        self.t.login('test_user')
        self.t.user_api.GetHomeTimeline = MagicMock(return_value=response)
        self.t._sync_tweetline()
        self.assertEqual(len(self.t.user.get_tweets()), 3)

    def test_update_timeline(self):
        pass

    def tearDown(self):
        alter_tables('drop_table')


class TestLoginRequiredDecorator(unittest.TestCase):
    def test_login_required_raises_exception(self):
        d = DumbClass()
        self.assertRaises(LoginRequired, d.dumb_method)


class TestParseTweets(unittest.TestCase):
    def setUp(self):
        self.t = TwitterApp(CONSUMER_KEY, CONSUMER_SECRET)

    def test_parse_tweet_response(self):
        try:
            with open('test_tweets_response.json') as f:
                response = json.load(f)
        except IOError:
            os.chdir(os.path.join(os.getcwd(), 'tests'))
            with open('test_tweets_response.json') as f:
                response = json.load(f)
        tweets = self.t._parse_tweets(response)
        self.assertEqual(len(tweets), 3)
        self.assertIsInstance(tweets[0], Tweet)


class TestUserModel(unittest.TestCase):
    def setUp(self):
        alter_tables()

    def test_username_exists(self):
        User.create(screen_name="test_user", id_user='',
                    oauth_token='', oauth_token_secret='')
        self.assertTrue(User.check_username_exists('test_user'))

    def test_username_doesnt_exist(self):
        self.assertFalse(User.check_username_exists('test_user'))

    def test_get_tweets_empty(self):
        u = User.create(screen_name="test_user", id_user='',
                        oauth_token='', oauth_token_secret='')
        self.assertEquals(u.get_tweets(), [])

    def test_get_tweets(self):
        u = User.create(screen_name="test_user", id_user='',
                        oauth_token='', oauth_token_secret='')
        t = Tweet.create(id_tweet='', created_at='',
                         user_screen_name='', user_name='',
                         text='', user=u)
        self.assertEquals(len(u.get_tweets()), 1)
        self.assertEquals(u.get_tweets(), [t])

    def test_get_last_tweet_id(self):
        u = User.create(screen_name="test_user", id_user='',
                        oauth_token='', oauth_token_secret='')
        kwargs = {
            'created_at': '',
            'user_screen_name': '',
            'user_name': '',
            'text': '',
            'user': u
        }
        Tweet.create(id_tweet='1', **kwargs)
        Tweet.create(id_tweet='2', **kwargs)
        self.assertEquals(u.get_last_tweet_id(), '2')

    def tearDown(self):
        alter_tables('drop_table')

if __name__ == '__main__':
    unittest.main()
